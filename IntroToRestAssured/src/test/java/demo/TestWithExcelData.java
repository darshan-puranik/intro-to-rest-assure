package demo;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import utils.Excel;

public class TestWithExcelData {
	
	static String URL = "https://reqres.in/api/users";
	static String filePath= "./data/TestData.xlsx";
	
	@Test
	public void post1(){
		
		Excel excelUtil = new Excel(filePath, "Sheet1", 0);
		Object[][] data = excelUtil.getAllData();
		
		for(int i=0;i<data.length;i++) {
				JSONObject jsonPostBodyObj = new JSONObject();
				jsonPostBodyObj.put("name", data[i][0].toString());
				jsonPostBodyObj.put("job", data[i][1].toString());
				
				given()
					.body(jsonPostBodyObj.toJSONString())
				.when()
				.post(URL)
				.then()
					.statusCode(201)
					.body("id", notNullValue());
		}
	}

}
