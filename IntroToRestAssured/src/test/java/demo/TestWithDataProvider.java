package demo;
import org.json.simple.JSONObject;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

public class TestWithDataProvider extends DataForTest {
	
	static String URL = "https://reqres.in/api/users";
	
	
	// This approach is for big data.
	// sampleData is defined in DataForTest
	@Test(dataProvider = "sampleData")
	public void post1(String firstName, String job){
	
		JSONObject jsonPostBodyObj = new JSONObject();
		jsonPostBodyObj.put("name", firstName);
		jsonPostBodyObj.put("job", job);
		
		given()
			.body(jsonPostBodyObj.toJSONString())
		.when()
		.post(URL)
		.then()
			.statusCode(201)
			.time(lessThan(2000L))
			.body("id", notNullValue())
			.log().body();
		
	}
	
	// This approach is for small amount of data.
	// Data come from XML file. There should be tag named parameter. 
	// You cant run this test from here. You have to go to testing.xml and run that file.
	@Parameters({"firstName", "job"})
	@Test
	public void post2(String firstName, String job){
		
		JSONObject jsonPostBodyObj = new JSONObject();
		jsonPostBodyObj.put("name", firstName);
		jsonPostBodyObj.put("job", job);
		
		given()
			.body(jsonPostBodyObj.toJSONString())
		.when()
		.post(URL)
		.then()
			.statusCode(201)
			.time(lessThan(2000L))
			.body("id", notNullValue())
			.log().body();
		
	}
}
