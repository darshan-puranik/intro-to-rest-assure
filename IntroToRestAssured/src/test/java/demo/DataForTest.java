package demo;
import org.testng.annotations.DataProvider;

public class DataForTest {
	@DataProvider(name = "sampleData")
	public Object[][] getData(){
		return new Object[][]{
				{"morpheus", "leader"}
				, {"danny", "servant"}
		};
	}
}
