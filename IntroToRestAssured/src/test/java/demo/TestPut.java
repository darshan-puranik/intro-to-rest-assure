package demo;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

import java.util.Date;

public class TestPut {
	
	static String URL = "https://reqres.in/api/users/2";
	
	@Test
	public void put1(){
	
		JSONObject jsonPostBodyObj = new JSONObject();
		jsonPostBodyObj.put("name", "morpheus");
		jsonPostBodyObj.put("job", "leader");
		
		given()
			.body(jsonPostBodyObj.toJSONString())
		.when()
		.put(URL)
		.then()
			.statusCode(200)
			.time(lessThan(2000L))
			.body("updatedAt", notNullValue());
		
	}
}
