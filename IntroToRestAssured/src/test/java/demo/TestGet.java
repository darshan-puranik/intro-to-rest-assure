package demo;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class TestGet {
	
	static String GET_URL= "https://reqres.in/api/users?page=2"; 
	
	@Test
	void test1 () {
		Response response = RestAssured.get(GET_URL);
		
		System.out.println(response.asPrettyString());
		System.out.println(response.getBody().asPrettyString());
		System.out.println(response.getStatusCode());
		
		int statusCode = response.getStatusCode();
		
		Assert.assertEquals(statusCode, 200);
	}
	
	@Test
	void test2() {
		given()
			.get(GET_URL)
		.then()
			.statusCode(200)
			.body("data.id[0]", equalTo(7));
	}
	
	@Test
	void test3() {
		given()	
			.get(GET_URL)
		.then()
			.statusCode(200)
			.body("data.first_name", hasItems("Michael","Lindsay"));
	}
}
