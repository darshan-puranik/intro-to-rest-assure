package demo;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

import java.util.Date;

public class TestDelete {
	
	static String URL = "https://reqres.in/api/users/2";
	
	@Test
	public void delete1(){
		given()
		.when()
		.delete(URL)
		.then()
			.statusCode(204)
			.time(lessThan(2000L));
		
	}
}
