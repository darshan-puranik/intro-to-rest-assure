package demo;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

public class TestPost {
	
	static String URL = "https://reqres.in/api/users";
	
	@Test
	public void post1(){
	
		JSONObject jsonPostBodyObj = new JSONObject();
		jsonPostBodyObj.put("name", "morpheus");
		jsonPostBodyObj.put("job", "leader");
		
		given()
			.body(jsonPostBodyObj.toJSONString())
		.when()
		.post(URL)
		.then()
			.statusCode(201)
			.time(lessThan(2000L))
			.body("id", notNullValue());
		
	}
}
