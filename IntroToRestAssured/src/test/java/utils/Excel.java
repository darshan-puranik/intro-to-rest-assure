package utils;


import java.io.IOException;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel {
	
	static XSSFWorkbook workbook;
	static XSSFSheet sheet;
	static int headerRowNumber;
	
	public Excel (String path, String sheetName, int headerRowNumber) {
		try {
			workbook = new XSSFWorkbook(path);
			sheet = workbook.getSheet(sheetName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public int getRowCount () {
		// class for XLSX format
		try {
			return sheet.getPhysicalNumberOfRows();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	public Object[][] getAllData () {
		// class for XLSX format
		try {
			// get number of rows
			int rowCount = sheet.getPhysicalNumberOfRows();
			
			// get number of columns
			Row row = sheet.getRow(headerRowNumber);
			int colmCount = row.getLastCellNum();
			
			// rowCount -1 because we dont want header row in data array.
			Object[][] array = new Object[rowCount-1][colmCount]; 
			DataFormatter formatter = new DataFormatter();
			
			for (int i=headerRowNumber+1; i<rowCount; i++) {
				for (int j=0; j<colmCount; j++) {
					// i-1 because excel has header row and data array doesnt.
					array[i-1][j] =formatter.formatCellValue(sheet.getRow(i).getCell(j));
				}
			}
			return array;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
